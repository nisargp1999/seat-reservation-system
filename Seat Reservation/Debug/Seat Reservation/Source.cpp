// Seat Reservation.cpp : Defines the entry point for the console application.
//

/*
Different Color Codes:
0   BLACK
1   BLUE
2   GREEN
3   CYAN
4   RED
5   MAGENTA
6   BROWN
7   LIGHTGRAY
8   DARKGRAY
9   LIGHTBLUE
10  LIGHTGREEN
11  LIGHTCYAN
12  LIGHTRED
13  LIGHTMAGENTA
14  YELLOW
15  WHITE
*/



#include <Windows.h>
#include <iostream>
#include <string>
#include <fstream>
using namespace std;

void readChart(string seatChart[10][4]);			//Done
void displaySeatChart(string seatChart[10][4]);		//Done
void reserveSeat();									//Not Done
void cancelReservation();							//Not Done
void saveChart(string saveChart[10][4]);			//Done
void Statistics();									//Not Done
void help();										//Done

													//--------------------------------------------------------------------------------------

int main()
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);
	int userInput;
	string seatChart[10][4];
	readChart(seatChart);




	cout << "Welcome. What would you like me to do?" << endl;
	cout << "Give me a number: " << endl;

	do {
		// ----------Menu----------
		cout << "----------Menu----------" << endl;
		cout << "1. Display Seat Chart" << endl;
		cout << "2. Reserve Seat" << endl;
		cout << "3. Cancel Reservation" << endl;
		cout << "4. Save Seat Chart to File" << endl;
		cout << "5. Statistics" << endl;
		cout << "6. Help" << endl;
		cout << "7. Quit" << endl;
		cout << "------------------------" << endl;
		// ----------Menu----------
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);
		cout << "...waiting for your input: " << endl;
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
		cin >> userInput;
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);
		system("cls");

		switch (userInput)
		{
		case 1:
			displaySeatChart(seatChart);
			break;
		case 4:
			saveChart(seatChart);
			break;
		case 6:
			help();
			break;
		default:
			cout << "Invalid Input. Please Try Again." << endl << endl;
		}
		system("cls");
	} while (userInput != 7);



	return 0;
}

//--------------------------------------------------------------------------------------

//---------------------------------------------------------
void readChart(string seatChart[10][4])
{
	string disregard;
	ifstream displayChart;
	displayChart.open("chartIn.txt");

	for (int row = 0; row < 10; row++)
	{
		for (int col = 0; col < 5; col++)
		{
			if (col == 0)
			{
				displayChart >> disregard;
			}
			else {
				displayChart >> seatChart[row][col - 1];
			}
		}
	}
	displayChart.close();
}

//----------------------------------------------------------

void displaySeatChart(string seatChart[10][4])
{
	for (int row = 0; row < 10; row++)
	{
		if (row == 9)
		{
			cout << row + 1 << " ";
		}
		else {
			cout << row + 1 << "  ";
		}
		for (int col = 0; col < 4; col++)
		{
			
			cout << seatChart[row][col];
		}
		cout << endl;
	}

	cout << endl;
	system("pause");
}

//------------------------------------------------------------

void saveChart(string seatChart[10][4])
{
	ofstream saveToFile;
	string fileName;
	cout << "Where would you like to save your seat reservations (File Name):" << endl;
	cin >> fileName;
	saveToFile.open(fileName);

	for (int row = 0; row < 10; row++)
	{
		saveToFile << row + 1 << " ";
		for (int col = 0; col < 4; col++)
		{
			saveToFile << seatChart[row][col];
		}
		saveToFile << endl;
	}

	cout << "Your seat reservation has been saved" << endl;
	saveToFile.close();
	system("pause");
}

//------------------------------------------------------------

void help()
{
	cout << "Help:" << endl << endl;

	cout << "Option 1: Display Seat Chart" << endl;
	cout << "---------------------------------------------------------" << endl;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 14);
	cout << "When you type 1, you will see the most updated seat chart\nwith 10 rows and 4 columns. 1st and last columns have window seats." << endl;
	cout << "---------------------------------------------------------" << endl;

	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);
	cout << endl << "Option 2: Reserve Seat" << endl;
	cout << "----------------------------------------------------------" << endl;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 14);
	cout << "When you type 2, you will be prompted for the desired seat\nthat you want to reserve. If the seat is available, a message will\nsay it is reserved. If the seat is not available, a message will say it is not." << endl;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
	cout << "Directions: Type the row number and the letter next to it showing which column the\nseat you want to reserve is located in." << endl;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 14);
	cout << "----------------------------------------------------------" << endl;

	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);
	cout << endl << "Option 3: Cancel Reservation" << endl;
	cout << "----------------------------------------------------------" << endl;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 14);
	cout << "When you type 3, you will be prompted for you seat number\nthat you to cancel. If you accidentally gave a wrong seat that is already available,\na message will give an error. If seat chosen for delition is reserved,\nprogram will cancel the seat and give you a message saying it is canceled" << endl;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
	cout << "Directions: Type the row number and the letter next to it showing\nwhich column the seat you want to cancel is located in." << endl;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 14);
	cout << "----------------------------------------------------------" << endl;

	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);
	cout << endl << "Option 4: Save Seat Chart to File" << endl;
	cout << "----------------------------------------------------------" << endl;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 14);
	cout << "When you type 4, you will be prompted for a file name. This\nallows the program to save your reserved seats in the particular file." << endl;
	cout << "----------------------------------------------------------" << endl;

	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);
	cout << endl << "Option 5: Statistics" << endl;
	cout << "----------------------------------------------------------" << endl;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 14);
	cout << "When you type 5, this program will display the following statistics:\n     1. Number of available seats\n     2. Percentage of seats that are reserved\n     3. List of available window seats\n     4. List of available aisle seats" << endl;
	cout << "----------------------------------------------------------" << endl;

	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);
	cout << endl << "Option 6: Help" << endl;
	cout << "----------------------------------------------------------" << endl;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 14);
	cout << "This option gives detailed message on how to use the program." << endl;
	cout << "----------------------------------------------------------" << endl;

	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);
	cout << endl << "Option 7: Quit" << endl;
	cout << "----------------------------------------------------------" << endl;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 14);
	cout << "A thank you message is displayed when number 7 is pressed and the program terminates" << endl;
	cout << "----------------------------------------------------------" << endl;



	system("pause");
}

//------------------------------------------------------------
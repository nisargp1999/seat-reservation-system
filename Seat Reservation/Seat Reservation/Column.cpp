#include "Column.h"

int column(string currentSeat)														//Function to convert user's input of current seat to column number
{
	string stringColumn;															//Variable to be used to cut down full seat string to last letter
	if (currentSeat.length() <= 2) {												//If current seat string is 2 lengths long
		stringColumn = currentSeat.substr(1, 1);									//Start from 2nd position and cut down until 1 length and saves it in stringColumn variable
	}
	else {																			//else if seat string is more than 2 lengths long
		stringColumn = currentSeat.substr(2, 1);									//Start from 3rd position and cut down until 1 length and saves it in stringColumn variable
	}
	int intColumn;																	//Variable to be used to return column number

	if (stringColumn == "A")														//If stringColumn matches A
		intColumn = 1;																		//Set column number to 1
	else if (stringColumn == "B")													//If stringColumn matches B
		intColumn = 2;																		//Set column number to 2
	else if (stringColumn == "C")													//If stringColumn matches C
		intColumn = 3;																		//Set column number to 3
	else if (stringColumn == "D")													//if stringColumn matches D
		intColumn = 4;																		//Set column number to 4
	else																			//else
		intColumn = -1;																		//Set column number to -1 to show error

	return intColumn;																//Return the column number
}
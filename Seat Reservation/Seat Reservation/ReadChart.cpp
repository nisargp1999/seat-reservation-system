#include "ReadChart.h"


void readChart(string seatChart[10][4])												//Reads the chart from file
{
	string disregard;																//Variable to disregard 1st column which only counts numbers in the file
	ifstream displayChart;
	displayChart.open("chartIn.txt");												//Opens the file

	for (int row = 0; row < 10; row++)												//Goes through all the rows
	{
		for 
			(int col = 0; col < 5; col++)											//Goes through all the columns
		{
			if (col == 0)															//If 1st column
			{
				displayChart >> disregard;													//Value is set to variable disregard
			}
			else {																	//else
				displayChart >> seatChart[row][col - 1];									//Saves the value in current row and column
			}
		}
	}
	displayChart.close();															//closes the file
}
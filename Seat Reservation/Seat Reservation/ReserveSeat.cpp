#include "ReserveSeat.h"
#include "Row.h"
#include "Column.h"

void reserveSeat(string seatChart[10][4])											//Function to reserve seat
{
#include "DisplaySeatChart.h"
	string currentSeat;																//Variable to be used for user's input
	int intRow;																		//Variable to be used for converted row number
	int intColumn;																	//Variable to be used for converted column number

	displaySeatChart(seatChart);													//Function called to display seat chart
	cout << endl << "Which seat would you like to reserve?\nAnswer must be row # and column letter typed together\nin resepective order:" << endl;			//Prompts user to give seat number
	cin >> currentSeat;																//Saves full seat number to variable currentSeat


	intRow = row(currentSeat);														//Saves converted row number to respected variable
	intColumn = column(currentSeat);												//Saves converted column number to respected variable

	if (seatChart[intRow - 1][intColumn - 1] == "X")								//If current array location has X
	{
		cout << "The seat is not available" << endl;										//Tells user seat is unavailable
	}
	else {																			//else
		seatChart[intRow - 1][intColumn - 1] = "X";											//Assigns X to current array location
		cout << "Your seat is reserved" << endl;											//Tells user seat has been reserved successfully
	}

	system("pause");
}
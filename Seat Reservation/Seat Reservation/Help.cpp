#include "Help.h"
#include <Windows.h>

void help()																											//Function to prints out detailed explaination
{
	cout << "Help:" << endl << endl;

	cout << "Option 1: Display Seat Chart" << endl;
	cout << "---------------------------------------------------------" << endl;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 14);
	cout << "When you type 1, you will see the most updated seat chart\nwith 10 rows and 4 columns. 1st and last columns have window seats." << endl;
	cout << "---------------------------------------------------------" << endl;

	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);
	cout << endl << "Option 2: Reserve Seat" << endl;
	cout << "----------------------------------------------------------" << endl;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 14);
	cout << "When you type 2, you will be prompted for the desired seat\nthat you want to reserve. If the seat is available, a message will\nsay it is reserved. If the seat is not available, a message will say it is not." << endl;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
	cout << "Directions: Type the row number and the letter next to it showing which column the\nseat you want to reserve is located in." << endl;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 14);
	cout << "----------------------------------------------------------" << endl;

	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);
	cout << endl << "Option 3: Cancel Reservation" << endl;
	cout << "----------------------------------------------------------" << endl;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 14);
	cout << "When you type 3, you will be prompted for you seat number\nthat you to cancel. If you accidentally gave a wrong seat that is already available,\na message will give an error. If seat chosen for delition is reserved,\nprogram will cancel the seat and give you a message saying it is canceled" << endl;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
	cout << "Directions: Type the row number and the letter next to it showing\nwhich column the seat you want to cancel is located in." << endl;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 14);
	cout << "----------------------------------------------------------" << endl;

	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);
	cout << endl << "Option 4: Save Seat Chart to File" << endl;
	cout << "----------------------------------------------------------" << endl;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 14);
	cout << "When you type 4, you will be prompted for a file name. This\nallows the program to save your reserved seats in the particular file." << endl;
	cout << "----------------------------------------------------------" << endl;

	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);
	cout << endl << "Option 5: Statistics" << endl;
	cout << "----------------------------------------------------------" << endl;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 14);
	cout << "When you type 5, this program will display the following statistics:\n     1. Number of available seats\n     2. Percentage of seats that are reserved\n     3. List of available window seats\n     4. List of available aisle seats" << endl;
	cout << "----------------------------------------------------------" << endl;

	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);
	cout << endl << "Option 6: Help" << endl;
	cout << "----------------------------------------------------------" << endl;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 14);
	cout << "This option gives detailed message on how to use the program." << endl;
	cout << "----------------------------------------------------------" << endl;

	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);
	cout << endl << "Option 7: Quit" << endl;
	cout << "----------------------------------------------------------" << endl;
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 14);
	cout << "A thank you message is displayed when number 7 is pressed and the program terminates" << endl;
	cout << "----------------------------------------------------------" << endl;



	system("pause");
}
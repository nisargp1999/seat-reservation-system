#include "CancelReservation.h"
#include "Row.h"
#include "Column.h"

void cancelReservation(string seatChart[10][4])										//Function to cancel a reservation
{
#include "DisplaySeatChart.h"
	string currentSeat;																//Variable to be used for user's input
	int intRow;																		//Variable to be used for converted row number
	int intColumn;																	//Variable to be used for converted column number

	displaySeatChart(seatChart);													//Function called to display seat chart
	cout << "Which seat would you like to cancel?\nAnswer must be row # and column letter typed together\nin resepective order:" << endl;		//Prompts user to give seat number
	cin >> currentSeat;																//Saves full seat number to variable currentSeat

	intRow = row(currentSeat);														//Saves current row number to respected variable
	intColumn = column(currentSeat);												//Saves current column number to respected variables

	if (seatChart[intRow - 1][intColumn - 1] != "X")								//If current array location does not have an X
	{
		cout << "The seat is not already reserved." << endl;								//Tells user seat is not already reserved
	}
	else {																			//else
		if (currentSeat.length() <= 2) {
			seatChart[intRow - 1][intColumn - 1] = currentSeat.substr(1, 1);
		}
		else {
			seatChart[intRow - 1][intColumn - 1] = currentSeat.substr(2, 1);
		}
		cout << "Your seat reservation has been canceled." << endl;							//Tells user seat reservation has been canceled
	}

	system("pause");
}

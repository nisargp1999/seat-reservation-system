

/*
Different Color Codes:
0   BLACK
1   BLUE
2   GREEN
3   CYAN
4   RED
5   MAGENTA
6   BROWN
7   LIGHTGRAY
8   DARKGRAY
9   LIGHTBLUE
10  LIGHTGREEN
11  LIGHTCYAN
12  LIGHTRED
13  LIGHTMAGENTA
14  YELLOW
15  WHITE
*/


#include "ReadChart.h"
#include "DisplaySeatChart.h"
#include "ReserveSeat.h"
#include "CancelReservation.h"
#include "SaveChart.h"
#include "Statistics.h"
#include "Help.h"
#include "Row.h"
#include "Column.h"

#include <Windows.h>
#include <iostream>
#include <string>
#include <fstream>

using namespace std;


int main()
{
	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);					//Color change
	int userInput;																	//Variable to be used for user's input
	string seatChart[10][4];														//Array of the seat chart
	readChart(seatChart);															//calls the function to read the chart in


	cout << "Welcome. What would you like me to do?" << endl;						//Welcome message
	cout << "Give me a number: " << endl;											//Prompts user for a number

	do {
		// ----------Menu----------													//Start of Menu...
		cout << "----------Menu----------" << endl;
		cout << "1. Display Seat Chart" << endl;
		cout << "2. Reserve Seat" << endl;
		cout << "3. Cancel Reservation" << endl;
		cout << "4. Save Seat Chart to File" << endl;
		cout << "5. Statistics" << endl;
		cout << "6. Help" << endl;
		cout << "7. Quit" << endl;
		cout << "------------------------" << endl;
		// ----------Menu----------													//...End of Menu
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);				//Color change
		cout << "...waiting for your input: " << endl;
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 15);
		cin >> userInput;															//Stores the user's input in userInput variable
		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 10);
		system("cls");																//Clears the screen with previous outputs

		switch (userInput)															//Determines which function to call according to what number user entered
		{
		case 1:																//If user entered 1...
			displaySeatChart(seatChart);											//Function called to display chart
			system("pause");
			break;
		case 2:																//If user entered 2...
			reserveSeat(seatChart);													//Function called to reserve a seat of user's choice
			break;
		case 3:																//If user entered 3...
			cancelReservation(seatChart);											//Function called to cancel a seat of user's choice
			break;
		case 4:																//If user entered 4...
			saveChart(seatChart);													//Function called to save the most updated seat chart
			break;
		case 5:																//If user entered 5...
			statistics(seatChart);													//Function called to print out statistics
			break;
		case 6:																//If user entered 6...
			help();																	//Function called to print out detailed explaination
			break;
		default:															//If user enters something other than these numbers, exept 7, program prints out Invalid input.
			cout << "Invalid Input. Please Try Again." << endl << endl;
		}
		system("cls");
	} while (userInput != 7);														//Code runs until user enters 7



	return 0;
}
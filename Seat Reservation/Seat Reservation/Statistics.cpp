#include "Statistics.h"

void statistics(const string saveChart[10][4])														//Function to print out statistics
{
	int count = 0;																					//Variable to be used count number of seats more than once
	for (int row = 0; row < 10; row++)																				//Goes through all the rows
	{
		for (int col = 0; col < 4; col++)																			//Goes through all the columns
		{
			if (saveChart[row][col] != "X")																			//If current array location does not have X
			{
				count++;																							//Count is increminated by 1
			}
		}
	}

	cout << "Number of available seats: " << count << endl;															//Prints out count variable
	cout << "Percentage of seats that are reserved: " << ((40 - count) / 40.0) * 100 << "%" << endl;				//Prints out calculated percentage of seats that are reserved

	cout << "List of window seats that are available:" << endl;
	for (int row = 0; row < 10; row++)																				//Goes through all the rows
	{
		for (int col = 0; col < 4; col += 3)																		//Goes through all the columns
		{
			if (saveChart[row][col] != "X")																			//If current array location does not have X
			{
				cout << row + 1 << saveChart[row][col] << endl;														//Prints out available window seats that are available
			}
		}
	}

	cout << "List of aisle seats that are available:" << endl;
	for (int row = 0; row < 10; row++)																				//Goes through all the rows
	{
		for (int col = 1; col < 3; col++)																			//Goes through all the columns
		{
			if (saveChart[row][col] != "X")																			//If current array location does not have X
			{
				cout << row + 1 << saveChart[row][col] << endl;														//Prints out list of aisle seats that are available
			}
		}
	}

	system("pause");

}
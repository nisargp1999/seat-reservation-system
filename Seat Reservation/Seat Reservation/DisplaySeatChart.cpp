#include "DisplaySeatChart.h"



void displaySeatChart(string seatChart[10][4])										//Function to display seat chart
{
	for (int row = 0; row < 10; row++)												//Goes through all the rows
	{
		if (row == 9)																//If at last row
		{
			cout << row + 1 << " ";															//prints out numbers top to bottom with 1 space
		}
		else {
			cout << row + 1 << "  ";														//else prints out numbers top to bottom with 2 spaces
		}
		for (int col = 0; col < 4; col++)											//Goes through all the columns
		{

			cout << seatChart[row][col] << " ";										//Prints out the value in that row and column from array
		}
		cout << endl;
	}

	cout << endl;
}
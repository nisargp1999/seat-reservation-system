#include "SaveChart.h"

void saveChart(string seatChart[10][4])																//Function to save chart to a file of user's choice
{
	ofstream saveToFile;
	string fileName;																				//Variable to be used for user's input of file name
	cout << "Where would you like to save your seat reservations (File Name):" << endl;				//Prompts user for a file name
	cin >> fileName;																				//Saves user's input in designated file
	saveToFile.open(fileName);																		//Opens the file

	for (int row = 0; row < 10; row++)																//Goes through all the rows
	{
		if (row == 9)																				//If at row 9
		{
			saveToFile << row + 1 << " ";															//Prints out numbers top to bottom with 1 space
		}
		else {																				//else
			saveToFile << row + 1 << "  ";															//Prints out numbers top to bottom with 2 spaces
		}
		for (int col = 0; col < 4; col++)															//Goes through all the columns
		{
			saveToFile << seatChart[row][col] << " ";												//Saves current location's value to the file
		}
		saveToFile << endl;
	}

	cout << "Your seat reservation has been saved" << endl;											//Tells the user seat reservation has been saved
	saveToFile.close();																				//Closes the file
	system("pause");
}
#include "Row.h"

int row(string currentSeat)															//Function to convert current seat to row number
{
	string stringRow = currentSeat.substr(0, 1);									//Cuts down string to 1 length
	int intRow;																		//Variable to be used to retern converted row number

	if (stringRow == "1" && currentSeat.length() <= 2)								//If currentSeat string is 2 length long
		intRow = 1;																			//Set row number to 1
	else if (stringRow == "2")														//If substring is equal to 2
		intRow = 2;																			//Set row number to 2
	else if (stringRow == "3")														//If substring is equal to 3
		intRow = 3;																			//Set row number to 3
	else if (stringRow == "4")														//If substring is equal to 4
		intRow = 4;																			//Set row number to 4
	else if (stringRow == "5")														//If substring is equal to 5
		intRow = 5;																			//Set row number to 5
	else if (stringRow == "6")														//If substring is equal to 6
		intRow = 6;																			//Set row number to 6
	else if (stringRow == "7")														//If substring is equal to 7
		intRow = 7;																			//Set row number to 7
	else if (stringRow == "8")														//If substring is equal to 8
		intRow = 8;																			//Set row number to 8
	else if (stringRow == "9")														//If substring is equal to 9		
		intRow = 9;																			//Set row number to 9
	else if (stringRow == "1")														//If substring is equal to 1 and if length of current seat string is more than 2
		intRow = 10;																			//Set row number to 10
	else
		intRow = -1;																//else set row number to -1 to show error

	return intRow;																	//Return row number
}